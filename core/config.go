package core

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var Configlist map[string]string

func init() {
	fmt.Println("start init config")
	Configlist = configInit()
	fmt.Println(Configlist)
}

func InitConfig()  {
	fmt.Println("personal init config")
	Configlist = configInit()
	fmt.Println(Configlist)

}

func configInit() map[string]string {
	conf := make(map[string]string)
	file, err := os.Open(".\\config.cfg")
	if err != nil {
		fmt.Println("open config failed:", err)
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	for {
		configR, _, err := reader.ReadLine()
		configread := string(configR)
		if err != nil {
			break
		}
		if strings.Contains(configread, "=") {
			splitstring := strings.Split(configread, "=")
			configkey := strings.TrimSpace(splitstring[0])
			configvalue := strings.TrimSpace(splitstring[1])
			conf[configkey] = configvalue
		}
	}
	return conf
}

func ConfigStr(str string) string {
	return Configlist[str]
}

func ConfigInt(str string) int {
	m := Configlist[str]
	t, _ := strconv.Atoi(m)
	return t
}

func ConfigInt64(str string) int64 {
	m := Configlist[str]
	t, _ := strconv.Atoi(m)
	s := int64(t)
	return s
}

func ConfigInt32(str string) int32 {
	m := Configlist[str]
	t, _ := strconv.Atoi(m)
	s := int32(t)
	return s
}